#Difference between apple and orange

#create or use a classifier which takes data as input and assigns label to its output
#example  Input "Image of an Apple" > "Classifier (Does the calculation and analysis)" > Outputs "APPLE"
import sklearn
from sklearn import tree
from cProfile import label

#STEP 1 :  Collect training data
#----------------------------------
#Tabular data for fruits (that contains features) to identify between apple and orange 

#features = [[140, "smooth"], [130, "smooth"], [510, "bumpy"],[170, "bumpy"]]  #input to the classifier contains its weight and skin type
#changed words to codes such as  0 for bumpy and 1 for smooth
#same for labels 0 for apples and 1 for orange


features = [[140, 1], [130, 1], [510, 0],[170, 0]]  #updated input to the classifier
labels = [0, 0, 1, 1] #output we want

# STEP 2 : Train the Classifier
#----------------------------------
# the classifier we chose is called DecisionTree, consider it as a Box Of Rules
clf = tree.DecisionTreeClassifier()  #at this stage classifier is does not know anything about apple or oranges yet
                                     # to train this classifier we need TrainingAlgorithm. If classifier is a box of rules,
                                     # then TrainingAlgorithm is a procedure that creates these rules. It does that by finding
                                     # patterns in your training data.

#clf = clf.fit(X, y, sample_weight, check_input, X_idx_sorted) #in sickit-learn TrainingAlgorithm is included within the classifier-
clf = clf.fit(features, labels)                                #object and its called "fit" which takes features and labels as input


#STEP 3 : Make predictions
#----------------------------------

#input to the classifier will be our input to the new example
print clf.predict([[160, 0]])

