#Difference between apple and orange

#create or use a classifier which takes data as input and assigns label to its output
#example  Input "Image of an Apple" > "Classifier (Does the calculation and analysis)" > Outputs "APPLE"
import numpy as np
import sklearn
from sklearn import tree
from cProfile import label
from sklearn.datasets.base import load_iris

#STEP 1 :  Collect training data 
#----------------------------------
#Tabular data for iris flower contains 150 different types of iris flower along with their features 
#sklearn gives provides an inbuilt iris libraries as an example using the function load_iris()
iris = load_iris()
features = iris.feature_names  #updated input to the classifier
labels = iris.target_names #output we want

test_idx = [0, 50, 100] #array index 0, 50, 100

#itrate through the iris data
for i in range(len(iris.target)):
    print "Example %d: label %s, features %s" % (i, iris.target[i], iris.data[i]) 

#diving training data in to two parts  
# 1-Training Data
train_target = np.delete(iris.target, test_idx) #removed the data from the total training array at index 0, 50 and 100 
train_data = np.delete(iris.data, test_idx, axis=0) #removed data from the total training array at index 0, 50 and 100


# 2-Testing data - Sub set of Training data to verify how accurate the classifier has predicted the results
test_target = iris.target[test_idx] # a new variable test_target to copy values from iris.target array at index 0, 50 and 100
test_data = iris.data[test_idx]     # a new variable test_data to copy values from iris.data array at index 0, 50 and 100




# STEP 2 : Train the Classifier
#----------------------------------
# the classifier we chose is called DecisionTree, consider it as a Box Of Rules
clf = tree.DecisionTreeClassifier()  #at this stage classifier is does not know anything about apple or oranges yet
#                                      # to train this classifier we need TrainingAlgorithm. If classifier is a box of rules,
#                                      # then TrainingAlgorithm is a procedure that creates these rules. It does that by finding
#                                      # patterns in your training data.
# 
# #clf = clf.fit(X, y, sample_weight, check_input, X_idx_sorted) #in sickit-learn TrainingAlgorithm is included within the classifier-
clf = clf.fit(train_data, train_target)                                #object and its called "fit" which takes features and labels as input


#STEP 3 : Make predictions
#----------------------------------

#input to the classifier will be our input to the new example
#copied target (label)
print "Original target value: " + str(test_target) 

#predicted target (label)
print "Predicted target value: " + str(clf.predict(test_data))



# (OPTIONAL) Visualise the tree
import pydotplus
from IPython.display import Image  
dot_data = tree.export_graphviz(clf, out_file=None, 
                         feature_names=iris.feature_names,  
                         class_names=iris.target_names,  
                         filled=True, rounded=True,  
                         special_characters=True)  
graph = pydotplus.graph_from_dot_data(dot_data)   
graph.write_pdf("iris.pdf") 

